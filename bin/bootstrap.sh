#!/usr/bin/env sh

LATEST_VER="registry.gitlab.com/thecb4/dockerandswift:latest"
MAJOR_VER="registry.gitlab.com/thecb4/dockerandswift:5-xenial"
MINOR_VER="registry.gitlab.com/thecb4/dockerandswift:5.0-xenial"

docker build --no-cache -t $LATEST_VER -t $MAJOR_VER -t $MINOR_VER .
# docker push $LATEST_VER && docker image rm $LATEST_VER
# docker push $MAJOR_VER && docker image rm $MAJOR_VER
# docker push $MINOR_VER && docker image rm $MINOR_VER
