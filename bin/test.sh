#!/usr/bin/env sh

# -v /var/run/docker.sock:/var/run/docker.sock

docker rm $(docker ps -a -q)
docker run -it --privileged --name "test-container" \
-v $PWD:/package \
-w /package \
-e REGISTRY_USER='thecb4' \
-e REGISTRY_PASSWORD=$REGISTRY_PASSWORD \
registry.gitlab.com/thecb4/dockerandswift:latest /bin/bash -c "bin/compile.sh"
