#!/usr/bin/env sh

LATEST_VER="registry.gitlab.com/thecb4/dockerandswift:test-image"

# echo $REGISTRY_PASSWORD >> pwd.txt \
# && \
echo $REGISTRY_PASSWORD | docker login registry.gitlab.com -u $REGISTRY_USER --password-stdin \
&& \
docker build \
  --no-cache \
  -f Dockerfile-swift \
  -t $LATEST_VER \
  . \
&& \
docker push $LATEST_VER \
&& \
rm -f pwd.txt
