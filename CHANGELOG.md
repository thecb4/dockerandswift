# Docker+Swift Change Log
All notable changes to this project will be documented in this file.

* Format based on [Keep A Change Log](https://keepachangelog.com/en/1.0.0/)
* This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.0] - 2019-APR-23 (build).
### Added
- Dockerfile (swift + docker)
- Dockerfile-swift (for testing)

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
- renamed gitlab-ci.yml to .gitlab-ci.yml

### Security
-
