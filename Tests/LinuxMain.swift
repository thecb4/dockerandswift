import XCTest

import DockerSwiftTests

var tests = [XCTestCaseEntry]()
tests += DockerSwiftTests.allTests()
XCTMain(tests)
