import XCTest
@testable import DockerSwift

final class DockerSwiftTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DockerSwift().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample)
    ]
}
