FROM swift:4.2

RUN mkdir -p /package
COPY deployment  /package
WORKDIR /package
RUN ls -alF /package

CMD ["./dockerswift"]
